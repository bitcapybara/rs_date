use chrono::prelude::*;
use clap::Parser;

#[derive(Debug, thiserror::Error)]
enum Error {
    #[error("timestamp parse error: {0:#}")]
    ParseTimestamp(#[from] std::num::ParseIntError),
    #[error("invalid timestamp length: {0}")]
    Length(usize),
    #[error("chrono error: {0:#}")]
    ParseDateTimeString(#[from] chrono::format::ParseError),
}

/// ts -f 1661436069
///
/// ts -f 2022-09-16 00:00:00
///
/// ts
#[derive(Debug, clap::Parser)]
struct Args {
    /// 传入时间戳，则转换为 2022-09-16 00:00:00 形式输出
    ///
    /// 传入 2022-09-16 00:00:00，则转换为时间戳输出
    ///
    /// 不传直接输出当前时间戳
    #[clap(short = 'f')]
    from: Option<String>,

    /// 时间戳单位，默认毫秒，指定 s 则为秒
    ///
    /// from 参数为空时有效
    #[clap(short = 's')]
    second: bool,
}

fn main() {
    let args = Args::parse();

    if let Err(e) = process(args) {
        println!("{0:#}", e)
    }
}

fn process(args: Args) -> Result<(), Error> {
    match args.from {
        Some(from) => {
            if from.chars().all(char::is_numeric) {
                // 时间戳转序列化字符串
                let timestamp = from.parse::<i64>()?;
                match from.len() {
                    10 => {
                        let datetime = Local.timestamp(timestamp, 0);
                        println!("{}", datetime.format("%Y-%m-%d %H:%M:%S"));
                    }
                    13 => {
                        let datetime = Local.timestamp_millis(timestamp);
                        println!("{}", datetime.format("%Y-%m-%d %H:%M:%S%.3f"));
                    }
                    n => return Err(Error::Length(n)),
                };
            } else {
                // 序列化字符串转时间戳
                let datetime = Local.datetime_from_str(&from, "%Y-%m-%d %H:%M:%S")?;
                println!("{}", datetime.timestamp())
            }
        }
        _ if args.second => println!("{}", Local::now().timestamp()),
        _ => println!("{}", Local::now().timestamp_millis()),
    }
    Ok(())
}
